/**
 * 
 */
package edu.hawaii.ics211;

/**
 * Tester class for the Calculator class
 * 
 * @author Chanelle Bealonis
 */
public class TestCalculator {

  /**
   * Tester for Calculator class
   * 
   * @param args, not used
   */
  public static void main(String[] args) {
    Calculator calc = new Calculator();
    System.out.println("Testing the add");
    System.out.println(calc.add(0, 0));
    System.out.println(calc.add(10, 40));
    System.out.println(calc.add(-1, 20));
    System.out.println(calc.add(-5, -10));
    System.out.println(calc.add(1, -100));
    System.out.println();

    System.out.println("Testing the subtract");
    System.out.println(calc.subtract(10, 40));
    System.out.println(calc.subtract(-1, 20));
    System.out.println(calc.subtract(-5, -10));
    System.out.println(calc.subtract(1, -100));
    System.out.println();

    System.out.println("Testing the multiply");
    System.out.println(calc.multiply(10, 40));
    System.out.println(calc.multiply(-1, 20));
    System.out.println(calc.multiply(-5, -10));
    System.out.println(calc.multiply(1, -100));
    System.out.println();

    System.out.println("Testing the mod");
    try {
      System.out.println(calc.modulo(0, 0));
    }
    catch (ArithmeticException e) {
      e.printStackTrace();
      System.out.println();
    }
    System.out.println(calc.modulo(0, 1));
    System.out.println(calc.modulo(10, 40));
    System.out.println(calc.modulo(300, 99));
    System.out.println(calc.modulo(-40, 3));
    System.out.println(calc.modulo(50, -9));
    System.out.println();

    System.out.println("Testing the pow");
    System.out.println(calc.pow(10, 40));
    System.out.println(calc.pow(0, 0));
    System.out.println(calc.pow(4, 4));
    System.out.println(calc.pow(3, 8));
    System.out.println(calc.pow(-3, 8));
    System.out.println(calc.pow(-8, 3));
    System.out.println();

    System.out.println("Testing the divide");
    System.out.println(calc.divide(10, 40));
    System.out.println(calc.divide(100, -100));
    System.out.println(calc.divide(200, 5));
    System.out.println(calc.divide(-200, -4));
    try {
      System.out.print(calc.divide(1, 0));
    }
    catch (ArithmeticException e) {
      e.printStackTrace();
      System.out.println();
    }
  }
}
